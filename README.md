# dolibarrApp_apk


## Getting started

Pour commencer, il faut que vous téléchargiez l'apk : [lien vers l'apk](Dolibarr_v2.apk).
Une fois téléchargé, il faut tout simplement l'installer.

Ensuite pour vous connecter voici les logs : 

- username : admin
- password : Lendellsup2

Quelques images pour vous montrer les étapes : 


<img src="img/1684720649085.jpg" width="250" height="500">

<img src="img/1684720649065.jpg" width="250" height="500">

<img src="img/1684773644304.jpg" width="250" height="500">

# Resultats

![alt text](img/cap1.png)
![alt text](img/cap2.png)


# Verification

Pour voir si tout est OK, vous devez vous rendre à l'adresse suivante : https://dolibarr.lendell.engineer/expensereport/list.php

Les identifiants pour vous connecter à Dolibarr sont les mêmes que pour l'application.

## Badges

![](https://img.shields.io/badge/status-up-brightgreen)
![](https://img.shields.io/badge/users-1-blue)
